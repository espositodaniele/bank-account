#include "header.h"

/*
 *  Function: operation_choise
 *  ----------------------------
 *  Returns (int)
 *
 *  Chiede all'utente di scegliere tra una delle opzioni del menu e ritorna il corrispondente valore
 *  della scelta.
 */


int operation_choise () {

    int choise;

    printf("\n******************************* MENU *******************************\n");
    printf("*                  SCEGLI UN OPERAZIONE DA EFFETUARE: \n");
    printf("*                     1. INSERISCI UN NUOVO UTENTE\n");
    printf("*                     2. VISUALIZZA LISTA MOVIMENTI\n");
    printf("*                             3. ESCI\n");
    printf("********************************************************************\n");
    printf("SELEZIONA LA TUA SCELTA: ");
    // Get operation choise FROM user
    scanf("%d", &choise);

    return choise;

};

/*
 *  Function: insert
 *  ----------------------------
 *  Returns (void)
 *
 *  input: @array users []
 *
 *  Function per inserire n utenti
 */

void insert(bank_account users[]) {

    int n_users = 0, n_transactions = 0;

    printf("QUANTI UTENTI VUOI INSERIRE? \n");
    scanf("%d", &n_users);

    for (int i = 0; i < n_users; ++i) {

        users[i].id = i+1;

        printf("INSERISCI NOME %d: ", i+1);
        scanf("%s", users[i].first_name);

        printf("INSERISCI COGNOME %d: ", i+1);
        scanf("%s", users[i].last_name);

        printf("INSERISCI NUMERO CONTO %d: ", i+1);
        scanf("%s", users[i].account_number);

        printf("QUANTI MOVIMENTI VUOI INSERIRE? \n");
        scanf("%d", &n_transactions);

        for (int j = 0; j < n_transactions; ++j) {

            printf("INSERISCI DESCRIZIONE MOVIMENTO %d: ", j+1);
            scanf("%s", users[i].transactions_list[j].description);

            printf("INSERISCI DATA (dd/mm/YYYY) %d: ", j+1);
            scanf("%s", users[i].transactions_list[j].date);


        } // fine for

        printf("\nCOMPLIMENTI, UTENTE INSERITO CON SUCCESSO \n");

    }; // Fine for

}

/*
 *  Function: insert
 *  ----------------------------
 *  Returns (int):
 *
 *  input: @array  users []
 *
 *  Function per la ricerca di un utente nel vettore users
 *  return -1 se non trova corrispondenze nel vettore altrimenti ritorna l'index dell'utente trovato
 */

int search_user(bank_account users[]) {

    char compare[30];

    printf("INSERIRE NOME/COGNOME O NUMERO CONTO UTENTE DA CERCARE: ");
    scanf("%s", compare);

    for (int i = 0; i < MAX_SIZE_ACCOUNTS; ++i) {

        if ( strcmp(users[i].first_name, compare) == 0 || strcmp(users[i].last_name, compare) == 0 || strcmp(users[i].account_number, compare) == 0  ) {

            return i;

        }

    } //fine for

    return -1;


};

/*
 *  Function: insert
 *  ----------------------------
 *  Returns (void):
 *
 *  input: @array  users []
 *
 *  Function per stampare la lista movimenti di un determinato utente in una fissata data
 */

void transaction_lists(bank_account users[], int user_index) {

    int i = 0, match = 0;
    char data[11];

    printf("INSERIRE DATA IN CUI SI VUOLE VISUALIZZARE LA LISTA MOVIMENTI (dd/mm/yyyy): ");
    scanf("%s", data);

    while (strcmp(users[user_index].transactions_list[i].description, "")) {

        if (strcmp(users[user_index].transactions_list[i].date, data) == 0 ){

            printf("DESCR: %s\n", users[user_index].transactions_list[i].description);
            printf("DATA: %s\n", users[user_index].transactions_list[i].date);

            match++;

        }
        i++;
    }

    if (match == 0) {

        printf("SPIECENTI, NON ABBIAMO TROVATO CORRISPONDENZA CON LA DATA INSERITA\n");

    }

};




