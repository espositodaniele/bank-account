#ifndef BANK_ACCOUNT_HEADER_H
#define BANK_ACCOUNT_HEADER_H

#include <stdio.h>
#include "string.h"

#define MAX_SIZE_DATE           20
#define MAX_SIZE_ACCOUNTS       50
#define MAX_SIZE_ACCOUNT_N      15
#define MAX_SIZE_LAST_NAME      20
#define MAX_SIZE_FIRST_NAME     10
#define MAX_SIZE_DESCRIPTION    30
#define MAX_SIZE_TRANSACTIONS   30



typedef struct {

    char description[MAX_SIZE_DESCRIPTION];

    char date[MAX_SIZE_DATE];

}transactions;


typedef struct {

    int id;

    char first_name[MAX_SIZE_FIRST_NAME];

    char last_name[MAX_SIZE_LAST_NAME];

    char account_number[MAX_SIZE_ACCOUNT_N];

    transactions transactions_list[MAX_SIZE_TRANSACTIONS];

} bank_account;


//funzione per la scelta della operazione da effettuare
int operation_choise ();

//Function per inserire un utente
void insert(bank_account users[]);

//Function per cercare un utente
int search_user(bank_account users[]);

// Function per mostrare lista movimento di un utente in una fissata data
void transaction_lists(bank_account users[], int );

#endif //BANK_ACCOUNT_HEADER_H
