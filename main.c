#include "header.h"


int main() {

    bank_account users_account[MAX_SIZE_ACCOUNTS]; //Vettore conto corrente utenti
    int user_index = -1, quit = 1;
    char other_operations;

    do {

        switch (operation_choise()) {

            case 1:
                insert(users_account); //Calling function to insert users. Input struct user_account
                break;
            case 2:
                user_index = search_user(users_account);
                //richiamo la funzione per cercare un utente e salvo il valore di ritorno. Input struct user_account

                if (user_index != -1) {

                    transaction_lists(users_account, user_index);   // Richiamo la funzione per stampare la lista dei movimenti.
                                                                    // Input struct user_account e index user trovato.

                } else {

                    printf("SPIACENTI, UTENTE NON TROVATO\n");

                }

                break;
            case 3:
                printf("ARRIVERDI, GRAZIE\n");
                break;

            default:
                printf("ATTENZIONE, SCELTA NON VALIDA\n");
        }

        printf("VUOI EFFETUARE ALTRE OPERAZIONI? (s/n): ");
        scanf(" %c", &other_operations);

        if (other_operations == 'n' || other_operations == 'N') {

            quit = 0;

        }

    } while (quit);

};



